const myHttp = require('./http');
const fs = require('mz/fs');

const server = myHttp.createServer();

server.on('request', (req, res) => {
  console.log({ 
    headers: req.headers,
    method: req.method,
    url: req.url,
  })
  res.setHeader('Content-Type', 'application/json');
  res.writeHead(200);
  fs.createReadStream('someFile.txt').pipe(res);
});

const PORT = 3030;

server.listen(PORT, () => {
  console.log(`Server is running on port: ${PORT}`);
});